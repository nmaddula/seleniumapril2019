package Selenium.Selenium_April;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumLibrary {

	public static WebDriver driver = null;

	public static void LaunchBrowserAndNavigateToUrl(String TestDataValue) {
		System.setProperty("webdriver.chrome.driver", "src/test/Drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(TestDataValue);
	}

	public static void MaximizingBrowser() {
		driver.manage().window().maximize();
	}

	public static boolean ElementPresenceAndEnableCheck(String LocatorValue) {
		if (WebelementToHandle(LocatorValue).isDisplayed() == true) {
			if (WebelementToHandle(LocatorValue).isEnabled() == true) {

				return true;

			} else {

				return false;
				
			}
		} else {

			return false;

		}
	}

	public static void ClickingAnElement(String LocatorValue) {

		if(ElementPresenceAndEnableCheck(LocatorValue) == true) {
			WebelementToHandle(LocatorValue).click();
		}
	}

	public static void EnteringText(String LocatorValue, String TestDataValue) {
		WebelementToHandle(LocatorValue).sendKeys(TestDataValue);
	}

	public static WebElement WebelementToHandle(String LocatorValue) {
		return driver.findElement(By.xpath(LocatorValue));
	}

}
