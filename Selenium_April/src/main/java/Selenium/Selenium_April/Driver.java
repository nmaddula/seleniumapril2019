package Selenium.Selenium_April;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Driver {

	public static void ExecuteAction(String TestActionValue, String LocatorValue, String TestDataValue) {

		AllActions ActionName = AllActions.valueOf(TestActionValue);

		try {

			Actions.ExecuteAction(ActionName, TestDataValue, LocatorValue);

		} catch (Exception e) {

			e.printStackTrace();

		}
	}

	public static void ExecuteAllTestCases(String TestCaseFolderPath) throws Throwable {

		File tcFolder = new File(TestCaseFolderPath);

		File[] allFiles = tcFolder.listFiles();

		for (File f : allFiles) {

			String TestCasePath = TestCaseFolderPath + f.getName();

			XSSFWorkbook testCaseFile = new XSSFWorkbook(TestCasePath);

			XSSFSheet testSheet = testCaseFile.getSheetAt(0);

			int TestDescriptionColumnNumber = -1, TestActionColumnNumber = -1, TestDataColumnNumber = -1,
					LocatorColumnNumber = -1, TagsColumnNumber = -1;

			int totalNumberofColumnsinHeaderRow = testSheet.getRow(0).getLastCellNum();

			/* Identifying the required columns presence */

			for (int i = 0; i < totalNumberofColumnsinHeaderRow; i++) {

				XSSFCell headerCell = testSheet.getRow(0).getCell(i);

				if (headerCell != null && !headerCell.toString().trim().isEmpty()) {

					String headerName = headerCell.toString().trim();

					if (headerName.equalsIgnoreCase("TestDescription")) {

						TestDescriptionColumnNumber = i;

					} else if (headerName.equalsIgnoreCase("TestAction")) {

						TestActionColumnNumber = i;

					} else if (headerName.equalsIgnoreCase("TestData")) {

						TestDataColumnNumber = i;

					} else if (headerName.equalsIgnoreCase("Locator")) {

						LocatorColumnNumber = i;

					} else if (headerName.equalsIgnoreCase("Tags")) {

						TagsColumnNumber = i;

					}
				}
			}

			/* Verifying the format of the Test Data provided and */
			if (TestDescriptionColumnNumber < 0) {

				throw new Exception("Test Description column is missing. Please check");
			}
			if (TestActionColumnNumber < 0) {

				throw new Exception("Test Description column is missing. Please check");
			}
			if (TestDataColumnNumber < 0) {

				throw new Exception("Test Description column is missing. Please check");
			}
			if (LocatorColumnNumber < 0) {

				throw new Exception("Test Description column is missing. Please check");
			}

			if (TagsColumnNumber < 0) {

				throw new Exception("Tags column is missing. Please check");
			}

			int totalRows = testSheet.getLastRowNum() + 1;

			/*
			 * Identifying each row's action and executing them
			 */

			for (int i = 1; i < totalRows; i++) {

				XSSFCell TestDescriptionCell, TestActionCell, TestDataCell, LocatorCell, TagsCell;
				String TestDescriptionValue = null, TestDataValue = null, LocatorValue = null, TestActionValue = null,
						TagsValue = null;

				TestDescriptionCell = testSheet.getRow(i).getCell(TestDescriptionColumnNumber);
				TestActionCell = testSheet.getRow(i).getCell(TestActionColumnNumber);
				TestDataCell = testSheet.getRow(i).getCell(TestDataColumnNumber);
				LocatorCell = testSheet.getRow(i).getCell(LocatorColumnNumber);
				TagsCell = testSheet.getRow(i).getCell(TagsColumnNumber);

				if (TestDescriptionCell != null && !TestDescriptionCell.toString().trim().isEmpty()) {
					TestDescriptionValue = TestDescriptionCell.toString().trim();
				}
				if (TestActionCell != null && !TestActionCell.toString().trim().isEmpty()) {
					TestActionValue = TestActionCell.toString().trim();
				}
				if (TestDataCell != null && !TestDataCell.toString().trim().isEmpty()) {
					TestDataValue = TestDataCell.toString().trim();
				}
				if (LocatorCell != null && !LocatorCell.toString().trim().isEmpty()) {
					LocatorValue = LocatorCell.toString().trim();
				}
				if (TagsCell != null && !TagsCell.toString().trim().isEmpty()) {
					TagsValue = TagsCell.toString().trim();
				}

				System.out.println(
						"*************************************************************************************");
				System.out.println("TestDescriptionValue:::::::::" + TestDescriptionValue);
				System.out.println("TestActionValue::::::::::::::" + TestActionValue);
				System.out.println("TestDataValue::::::::::::::::" + TestDataValue);
				System.out.println("LocatorValue:::::::::::::::::" + LocatorValue);
				System.out.println(
						"*************************************************************************************");

				if (TagsValue != null) {

					if (!TagsValue.trim().equalsIgnoreCase("Ignore")) {

						ExecuteAction(TestActionValue, LocatorValue, TestDataValue);
					}

				} else {

					ExecuteAction(TestActionValue, LocatorValue, TestDataValue);

				}

			}

			testCaseFile.close();
		}
	}

	public static void main(String[] args) throws Throwable {
		// adding a new line to check my code changes --sujana
		// adding second change

		// Fetching all properties from config.properties file

		Properties prop = new Properties();

		prop.load(new FileInputStream(new File("src/test/Configurations/config.properties")));

		String TestCaseFolderPath = prop.getProperty("TestCaseFolderPath");
		
		ExecuteAllTestCases(TestCaseFolderPath);

	}

}
